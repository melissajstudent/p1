09/11/2015

# Project 1

This project demonstrates that I have been successful setting up the environments, developing a site, and navigating/updating between local, remote, and live servers.

My site leverages the Bootstrap framework.

Project 1 live url: http://mjproductions.biz